import numpy as np
import glm

import trimesh
import Mesh as m

def globalToLocalMeshUnion(mesh1, mesh2, proximalEntity, distalEntity, enableTrimeshProcess=False):

    # Tests
    proximalTri = trimesh.Trimesh(vertices=mesh1.getVertices(),faces=mesh1.getIndices(),process=False) # /!\ conversion éventuelle inconnue, apparemment non d'après Cong-Lap
    distalTri = trimesh.Trimesh(vertices=mesh2.getVertices(),faces=mesh2.getIndices(),process=False)

    # Application des global transform pour avoir les coordonnées de local en global

    print(proximalEntity["global_transform"])
    print(distalEntity["global_transform"])

    def globalToLocal(entity):

        # Using glm
        position = glm.vec3(entity["global_transform"]["position"])
        o                   = glm.vec4(entity["global_transform"]["orientation"])
        orientation    = glm.quat(o.w, o.x, o.y, o.z)        
        scale          = glm.vec3(entity["global_transform"]["scale"])
        globalMatrix  = glm.translate(glm.mat4(1), position)
        globalMatrix *= glm.mat4_cast(glm.inverse(orientation))
        globalMatrix  = glm.scale(globalMatrix, scale)
        # correction of translations :
        for i in range(3):
            (globalMatrix[i,3], globalMatrix[3,i]) = (globalMatrix[3,i], globalMatrix[i,3])
        return globalMatrix*glm.mat4(1.0) # glm.mat4(1.0) correspond au repère local, toujours défini en 0,0,0 donc matrice identité

    proximalMatrix = globalToLocal(proximalEntity)
    distalMatrix = globalToLocal(distalEntity)
    proximalTri.apply_transform(proximalMatrix)
    distalTri.apply_transform(distalMatrix)
    print(distalMatrix)

    meshes = [proximalTri, distalTri]

    # First version (faster but don't fix manifolds, normals, ...)
    vertice_list = [mesh.vertices for mesh in meshes]
    faces_list = [mesh.faces for mesh in meshes]
    faces_offset = np.cumsum([v.shape[0] for v in vertice_list])
    faces_offset = np.insert(faces_offset, 0, 0)[:-1]

    vertices = np.vstack(vertice_list)
    faces = np.vstack([face + offset for face, offset in zip(faces_list, faces_offset)])
    merged_meshes = trimesh.Trimesh(vertices, faces, process=enableTrimeshProcess)


    merged_3dverse = m.mesh(0, "merged_3dverse")
    merged_3dverse.setVertices(np.array(np.array(merged_meshes.vertices).flatten(), dtype=np.float32))
    merged_3dverse.setNormals(np.array(np.array(merged_meshes.vertex_normals).flatten(), dtype=np.float32))
    merged_3dverse.setIndices(np.array(np.array(merged_meshes.faces).flatten(), dtype=np.uint32))

    return merged_3dverse